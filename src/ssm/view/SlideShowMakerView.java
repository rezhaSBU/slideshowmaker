package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.TOOLTIP_ADD_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_EXIT;
import static ssm.LanguagePropertyType.TOOLTIP_LOAD_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_DOWN;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_UP;
import static ssm.LanguagePropertyType.TOOLTIP_NEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_SAVE_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_VIEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_NAME_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_NEXT_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_PREVIOUS_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_REMOVE_SLIDE;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static ssm.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.ICON_ADD_SLIDE;
import static ssm.StartupConstants.ICON_EXIT;
import static ssm.StartupConstants.ICON_LOAD_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_MOVE_DOWN;
import static ssm.StartupConstants.ICON_MOVE_UP;
import static ssm.StartupConstants.ICON_NEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_SAVE_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_VIEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_NAME_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.ICON_REMOVE_SLIDE;
import static ssm.StartupConstants.ICON_WINDOW;
import static ssm.StartupConstants.LABEL_SLIDE_SHOW_TITLE;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.controller.FileController;
import ssm.controller.ImageSelectionController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 * This class provides the User Interface for this application,
 * providing controls and the entry points for creating, loading, 
 * saving, editing, and viewing slide shows.
 * 
 * @author McKilla Gorilla & _____________
 */
public class SlideShowMakerView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newSlideShowButton;
    Button loadSlideShowButton;
    Button saveSlideShowButton;
    Button viewSlideShowButton;
    Button nameSlideShowButton;
    Button exitButton;
    Label slideTitle;
    // WORKSPACE
    HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button addSlideButton;
    Button removeSlideButton;
    Button moveUpButton;
    Button moveDownButton;
    
    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideShowModel slideShow;
    BorderPane ssvPane;
    Button nextButton;
    Button previousButton;
    Image image;
    ImageView imv;
    ImageSelectionController imc;
    int sli; //used for slideshow viewer indexing
    //Stage secondaryStage;
    
    
    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public SlideShowMakerView(SlideShowFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// MAKE THE DATA MANAGING MODEL
	slideShow = new SlideShowModel(this);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
    }

    // ACCESSOR METHODS
    public SlideShowModel getSlideShow() {
	return slideShow;
    }

    public Stage getWindow() {
	return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle);
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	slideEditToolbar = new VBox();
	slideEditToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
	addSlideButton = this.initChildButton(slideEditToolbar,ICON_ADD_SLIDE,
                TOOLTIP_ADD_SLIDE,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
      	removeSlideButton = this.initChildButton(slideEditToolbar,ICON_REMOVE_SLIDE,
                TOOLTIP_REMOVE_SLIDE,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
	moveUpButton = this.initChildButton(slideEditToolbar,ICON_MOVE_UP,
                TOOLTIP_MOVE_UP,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
	moveDownButton = this.initChildButton(slideEditToolbar,ICON_MOVE_DOWN,
                TOOLTIP_MOVE_DOWN,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
	
	// AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox();
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
        
	// NOW PUT THESE TWO IN THE WORKSPACE
	workspace.getChildren().add(slideEditToolbar);
	workspace.getChildren().add(slidesEditorScrollPane);
    }

    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	fileController = new FileController(this, fileManager);
	newSlideShowButton.setOnAction(e -> {
	    fileController.handleNewSlideShowRequest();
	});
	loadSlideShowButton.setOnAction(e -> {
	    fileController.handleLoadSlideShowRequest();
	});
	saveSlideShowButton.setOnAction(e -> {
	    fileController.handleSaveSlideShowRequest();
	});
        viewSlideShowButton.setOnAction(e -> {
                initSSV();
	});
        nameSlideShowButton.setOnAction(e -> {
	    fileController.handleNameSlideShowRequest();
	});
	exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
	});
	
	// THEN THE SLIDE SHOW EDIT CONTROLS
	editController = new SlideShowEditController(this);
	addSlideButton.setOnAction(e -> {
	    editController.processAddSlideRequest();
	});
        removeSlideButton.setOnAction(e -> {
	    editController.processRemoveSlideRequest();
	});
        moveUpButton.setOnAction(e -> {
	    editController.processMoveUpRequest();
	});
        moveDownButton.setOnAction(e -> {
	    editController.processMoveDownRequest();
	});
        
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	newSlideShowButton = initChildButton(fileToolbarPane, ICON_NEW_SLIDE_SHOW,	TOOLTIP_NEW_SLIDE_SHOW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadSlideShowButton = initChildButton(fileToolbarPane, ICON_LOAD_SLIDE_SHOW,	TOOLTIP_LOAD_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	saveSlideShowButton = initChildButton(fileToolbarPane, ICON_SAVE_SLIDE_SHOW,	TOOLTIP_SAVE_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	viewSlideShowButton = initChildButton(fileToolbarPane, ICON_VIEW_SLIDE_SHOW,	TOOLTIP_VIEW_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	nameSlideShowButton = initChildButton(fileToolbarPane, ICON_NAME_SLIDE_SHOW,	TOOLTIP_NAME_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        slideTitle = new Label("     " + LABEL_SLIDE_SHOW_TITLE);
        fileToolbarPane.getChildren().add(slideTitle);
    }

    
    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);
        
        //SET ICON FOR THE WINDOW
        primaryStage.getIcons().add(new Image("file:" + PATH_ICONS + ICON_WINDOW));
        
	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ssmPane = new BorderPane();
	ssmPane.setTop(fileToolbarPane);	
	primaryScene = new Scene(ssmPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
        
    }

    /*
    *THIS IS WHERE THE SLIDESHOW WILL BE VIEWED   
    */
    private void initSSV() {
        //Pane where everything is laid out
        ssvPane = new BorderPane();
        sli = 0;
        //Next Slide Button
        nextButton = this.initViewerButton(ICON_NEXT,
        TOOLTIP_NEXT_SLIDE,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        ssvPane.setRight(nextButton);
        //Previous Slide Button
        previousButton = this.initViewerButton(ICON_PREVIOUS,
        TOOLTIP_PREVIOUS_SLIDE,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);        
        ssvPane.setLeft(previousButton);
        //Title
        ssvPane.setTop(new Label("          " + slideTitle.getText()));
        //Slide's image get image, puts in ImageView, sets to Pane's center
        File file = new File(PATH_SLIDE_SHOW_IMAGES + slideShow.getSlides().get(0).getImageFileName());
        image = new Image(file.toURI().toString());
        imv = new ImageView(image);
        imv.setFitWidth(450);
        imv.setFitHeight(360);
        ssvPane.setCenter(imv);
        //Caption
        ssvPane.setBottom(new Label("               Fig. " + (sli+1) + " " 
                + slideShow.getSlides().get(sli).getCaption()));
        
        //THEN THE SLIDESHOW VIEWER CONTROLS
        nextButton.setOnAction(e -> {
	    if(sli > slideShow.getLength()-2){
                sli = 0;
            }else sli++;
            File filer = new File(PATH_SLIDE_SHOW_IMAGES + slideShow.getSlides().get(sli).getImageFileName());
            image = new Image(filer.toURI().toString());
            imv = new ImageView(image);
            imv.setFitWidth(450);
            imv.setFitHeight(360);            
            ssvPane.setCenter(imv);
            ssvPane.setBottom(new Label("               Fig. " + (sli+1) + " "
                    + slideShow.getSlides().get(sli).getCaption()));
	});
        previousButton.setOnAction(e -> {
	    if(sli < 1){
                sli = slideShow.getLength()-1;
            }else sli--;
            File filer = new File(PATH_SLIDE_SHOW_IMAGES + slideShow.getSlides().get(sli).getImageFileName());
            image = new Image(filer.toURI().toString());
            imv = new ImageView(image);
            imv.setFitWidth(450);
            imv.setFitHeight(360);
            ssvPane.setCenter(imv);
            ssvPane.setBottom(new Label("               Fig. " + (sli+1) + " " 
                    + slideShow.getSlides().get(sli).getCaption()));
        });
                         
        Scene secondScene = new Scene(ssvPane, 550, 500);

        Stage secondStage = new Stage();
        secondStage.setTitle("SlideShow Viewer");
        secondStage.setScene(secondScene);

        //Set position of second window, related to primary window.
        secondStage.setX(200);
        secondStage.setY(200);

        newSlideShowButton.setDisable(true);
	loadSlideShowButton.setDisable(true);
	saveSlideShowButton.setDisable(true);
	viewSlideShowButton.setDisable(true);
	nameSlideShowButton.setDisable(true);
        exitButton.setDisable(true);
        secondStage.showAndWait();
        updateToolbarControls(false);
        newSlideShowButton.setDisable(false);
	loadSlideShowButton.setDisable(false);
    }
    
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    
    public Button initViewerButton(
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	return button;
    }
    
    /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	ssmPane.setCenter(workspace);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveSlideShowButton.setDisable(saved);
	viewSlideShowButton.setDisable(false);
        nameSlideShowButton.setDisable(saved);
	// AND THE SLIDESHOW EDIT TOOLBAR
	addSlideButton.setDisable(false);
       	removeSlideButton.setDisable(saved);
        moveUpButton.setDisable(false);
        moveDownButton.setDisable(false);
        
        slideTitle.setText("     " + LABEL_SLIDE_SHOW_TITLE);
    }

    /**
     * Uses the slide show data to reload all the components for
     * slide editing.
     * 
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSlideShowPane(SlideShowModel slideShowToLoad) {
	slidesEditorPane.getChildren().clear();
	for (Slide slide : slideShowToLoad.getSlides()) {
	    SlideEditView slideEditor = new SlideEditView(slide);
            if(slide.equals(slideShowToLoad.getSelectedSlide())){
                slideEditor.setStyle("-fx-border-color: cyan");
            }
	    slidesEditorPane.getChildren().add(slideEditor);

            slideEditor.setOnMouseClicked(e -> {
                for (int i = 0; i < slidesEditorPane.getChildren().size(); i++){
                    slidesEditorPane.getChildren().get(i).setStyle("-fx-border-color: rgb(70, 5, 5);");
                }
                slideShowToLoad.setSelectedSlide(slide);
                slideEditor.setStyle("-fx-border-color: cyan");
            });

	}
    }
}
