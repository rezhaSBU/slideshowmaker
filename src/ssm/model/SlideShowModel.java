package ssm.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import ssm.view.SlideShowMakerView;

/**
 * This class manages all the data associated with a slideshow.
 * 
 * @author McKilla Gorilla & _____________
 */
public class SlideShowModel {
    SlideShowMakerView ui;
    String title;
    ObservableList<Slide> slides;
    Slide selectedSlide;
    private Object slidesEditorPane;
    
    public SlideShowModel(SlideShowMakerView initUI) {
	ui = initUI;
	slides = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
	return selectedSlide != null;
    }
    
    public ObservableList<Slide> getSlides() {
	return slides;
    }
    
    public Slide getSelectedSlide() {
	return selectedSlide;
    }

    public String getTitle() { 
	return title; 
    }
    
    public int getLength() {
        return slides.size();
    }
    
    // MUTATOR METHODS
    public void setSelectedSlide(Slide initSelectedSlide) {
	selectedSlide = initSelectedSlide;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
        slides.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
	selectedSlide = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    public void addSlide(   String initImageFileName,
			    String initImagePath,
                            String initCaption) {
	Slide slideToAdd = new Slide(initImageFileName, initImagePath, initCaption);
	slides.add(slideToAdd);
	selectedSlide = slideToAdd;
	ui.reloadSlideShowPane(this);
    }
    
     /**
     * Removes a selected slide from the slide show.
     */
    public void removeSlide() {
	Slide slideToRemove = getSelectedSlide();
        slides.remove(slideToRemove);
        setSelectedSlide(slides.get(0));
	ui.reloadSlideShowPane(this);
    }
    
     /**
     * Moves a slide up in the slide show.
     */
    public void moveUp() {
	Slide slideToMove = getSelectedSlide();
        int i = slides.indexOf(slideToMove);
        if(i > 0){
            slides.remove(slideToMove);
            slides.add(i-1, slideToMove);
        }
	ui.reloadSlideShowPane(this);
    }    
    
     /**
     * Moves a slide down in the slide show.
     */
    public void moveDown() {
	Slide slideToMove = getSelectedSlide();
        int i = slides.indexOf(slideToMove);
        if(i < slides.size()-1){
            slides.remove(slideToMove);
            slides.add(i+1, slideToMove);
        }
	ui.reloadSlideShowPane(this);
    }    
}